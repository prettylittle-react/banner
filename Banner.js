import React from 'react';
import PropTypes from 'prop-types';

//import Img from 'img';
import Container from 'container';
import {getModifiers} from 'libs/component';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import './Banner.scss';

/**
 * Banner
 * @description [Description]
 * @example
  <div id="Banner"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Banner, {
        title : 'Example Banner'
    }), document.getElementById("Banner"));
  </script>
 */
class Banner extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'banner';
	}

	render() {
		const {className, id, image, width, children, theme, alignX, alignY, hasFixedBackground} = this.props;

		const atts = {
			className:
				getModifiers(this.baseClass, [alignX, alignY, hasFixedBackground ? 'fixed' : null]) + ` ${className}`,
			id: id || null
		};

		if (theme) {
			atts['data-theme'] = theme;
		}

		return (
			<div {...atts} ref={component => (this.component = component)}>
				<Img {...image} className={`${this.baseClass}__image`} useAsBackground={true} />
				<Container width={width}>
					<div className={`${this.baseClass}__main`}>{children}</div>
				</Container>
			</div>
		);
	}
}

Banner.defaultProps = {
	className: '',
	hasFixedBackground: false,
	alignX: 'left',
	alignY: '',
	theme: null,
	children: null
};

Banner.propTypes = {
	className: PropTypes.string,
	hasFixedBackground: PropTypes.bool,
	alignX: PropTypes.string,
	alignY: PropTypes.string,
	theme: PropTypes.string,
	children: PropTypes.node
};

export default Banner;
